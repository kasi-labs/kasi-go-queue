package message

import "context"

type Payload []byte

type Message struct {
	UUID     string
	Metadata Metadata

	Payload Payload

	ctx context.Context
}

func NewMessage(uuid string, payload Payload) *Message {
	return &Message{
		UUID:     uuid,
		Metadata: make(map[string]string, 0),
		Payload:  payload,
	}
}

// Unmarshaler is the interface implemented by types
// that can unmarshal a Message Payload description of themselves.
// The input can be assumed to be a valid encoding of
// a Message Payload value. UnmarshalPayload must copy the Message Payload data
// if it wishes to retain the data after returning.
//
// By convention, to approximate the behavior of Unmarshal itself,
// Unmarshalers implement UnmarshalPayload([]byte("null")) as a no-op.
type Unmarshaler interface {
	UnmarshalPayload([]byte) error
}

// Marshaler is the interface implemented by types that
// can marshal themselves into valid Message Payload.
type Marshaler interface {
	MarshalPayload() ([]byte, error)
}
